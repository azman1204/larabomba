<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class TestMiddleware {
    public function handle(Request $request, Closure $next) {
        // check() - semak user dah login / belum. return true / false
        if (! Auth::check()) {
            return redirect('login')->with('msg', 'You must login first');
        }
        return $next($request);
    }
}
