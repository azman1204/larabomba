<?php
namespace App\Http\Controllers;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    // list all posts
    function list() {
        //$posts = Post::all();
        // 1 page, show 3 records
        $posts = Post::paginate(3);
        $no = $posts->firstItem();// firstItem() return running no
        return view('post.list', compact('posts', 'no'));
    }

    // create a new post
    function create() {
        $post = new Post();
        return view('post.form', compact('post'));
    }

    // edit a post
    function edit($id) {
        $post = Post::find($id); // cari by pk
        return view('post.form', compact('post'));
    }

    // insert / update a post
    function save(Request $req) {
        // read all data from form
        $id = $req->id;

        if (! empty($id)) {
            // record exist, update
            $post = Post::find($id);
        } else {
            // record does not exist, create new
            $post =new Post();
        }

        // set data into model Post
        $post->title       = $req->title;
        $post->description = $req->description;
        $post->content     = $req->content;
        $post->category_id = $req->category;
        $post->posted      = $req->posted;
        $post->slug        = \Illuminate\Support\Str::slug($req->title, '-');
        // validation
        $rules = [
            'title' => 'required|min:5|max:500',
            'description' => 'required',
        ];

        if ($req->validate($rules)) {
            // validation ok
            $post->save();
            return redirect('post/list');
        }
        // if validation not ok, auto redirect to prev form
    }

    // delete a post
    function delete($id) {
        if(session('role') !== 'admin')
            echo 'No permission'; exit;

        Post::find($id)->delete();
        return redirect('post/list');
    }
}
