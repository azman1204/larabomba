<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller {
    // show login form
    function index() {
        return view('login');
    }

    // do authentication logic
    function auth(Request $req) {
        $user_id  = $req->user_id;
        $password = $req->password;
        echo "$user_id $password";
        // User::find($id); User::all(); User::paginate(3);
        // User::where('user_id', $user_id)->get() OR User::where('user_id', $user_id)->first()
        $user = User::where('user_id', $user_id)->first(); // first() return an object / User
        // dlm php null, empty string, 0 , false = FALSE
        if (! $user) {
            // user x wujud
            return redirect('login')->with('msg', 'Wrong combination of username and password');
        } else {
            // user wujud, semak password
            if (Auth::attempt(['user_id' => $user_id, 'password' => $password])) {
                // berjaya
                session(['role' => $user->role]); // set session variable
                return redirect('/');
            } else {
                // tidak berjaya
                return redirect('login')->with('msg', 'Wrong combination of username and password');
            }
        }
    }

    function signout() {
        Auth::logout();
        session()->flush(); // destroy all session
        return redirect('login');
    }
}
