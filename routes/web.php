<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
Route::get('/emel', function() {
    // ../views/sample.blade.php
    Mail::send('sample', ['name' => 'azman'], function($message) {
        $message->to('azman1204@yahoo.com');
        $message->subject('Emel Testing');
        $message->from('admin@bomba.gov.my');
    });
});

Route::get('/login', [LoginController::class, 'index']);
Route::post('/auth', [LoginController::class, 'auth']);
Route::get('/signout', [LoginController::class, 'signout']);

Route::get('/', function () {
    return view('welcome');
});

// jana password
Route::get('/gen-password', function () {
    echo Hash::make('1234');
});

Route::group(['middleware' => 'test'], function() {
    // http://larabomba.test/post/list
    Route::get('/post/list', [PostController::class, 'list']);
    Route::get('/post/create', [PostController::class, 'create']);
    Route::post('/post/save', [PostController::class, 'save']);
    Route::get('/post/edit/{id}', [PostController::class, 'edit']);
    Route::get('/post/delete/{id}', [PostController::class, 'delete']);
});




