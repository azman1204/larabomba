@extends('master')
@section('title', 'Post List')
@section('content')
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Title</th>
                <th>Description</th>
                <th>Posted</th>
                <th>Category</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @php $cat = ['1' => 'Article', '2' => 'News', '0' => '', '' => '']; @endphp
            @foreach($posts as $p)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $p->title }}</td>
                <td>{{ $p->description }}</td>
                <td>{{ $p->posted }}</td>
                <td>{{ $cat[$p->category_id] }}</td>
                <td>
                    <a href="{{ url('post/edit/' . $p->id) }}" class="btn btn-primary btn-sm">Edit</a>

                    @if(session('role') == 'admin')
                        <a href="{{ url('post/delete/' . $p->id) }}"
                        class="btn btn-danger btn-sm" onclick="return confirm( 'Are you sure ?' )">Delete</a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $posts->links() }}
@endsection
