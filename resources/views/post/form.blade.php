@extends('master')
@section('title', 'New Post')
@section('content')

<form action="{{ url('post/save') }}" method="post">
    <input type="hidden" name="id" value="{{ $post->id }}">
    @csrf

    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }} <br>
            @endforeach
        </div>
    @endif

    <div class="row mb-1">
        <div class="col-md-2">Title</div>
        <div class="col-md-8">
            <input type="text" class="form-control" name="title" value="{{ old('title', $post->title) }}">
        </div>
    </div>
    <div class="row mb-1">
        <div class="col-md-2">Description</div>
        <div class="col-md-8">
            <textarea class="form-control" name="description">{{ old('description', $post->description) }}</textarea>
        </div>
    </div>
    <div class="row mb-1">
        <div class="col-md-2">Content</div>
        <div class="col-md-8">
            <textarea class="form-control" name="content">{{ old('content', $post->content) }}</textarea>
        </div>
    </div>
    <div class="row mb-1">
        <div class="col-md-2">Category</div>
        <div class="col-md-8">
            <select name="category" class="form-control">
                <option value="0">-- Please Choose --</option>
                <option value="1" @if(old('category', $post->category_id) == '1') selected @endif>Article</option>
                <option value="2" @if(old('category', $post->category_id) == '2') selected @endif>News</option>
            </select>
        </div>
    </div>
    <div class="row mb-1">
        <div class="col-md-2">Posted ?</div>
        <div class="col-md-8">
            <select name="posted" class="form-control">
                <option value="0">-- Please Choose --</option>
                <option value="YES" @if(old('posted', $post->posted) == 'YES') selected @endif>Yes</option>
                <option value="NOT" @if(old('posted', $post->posted) == 'NOT') selected @endif>No</option>
            </select>
        </div>
    </div>
    <div class="row mb-1">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <input type="submit" value="Save" class="btn btn-primary">
        </div>
    </div>
</form>
@endsection
